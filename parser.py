# ~*~ coding: utf-8 ~*~
import argparse

parser = argparse.ArgumentParser()

# настройки
# group1 = parser.add_argument_group('process', "Настройки")


# обработка файлов
parser.add_argument('--cmd', choices=['conf', 'process'], help="Команда", type=str)

group1 = parser.add_argument_group('conf', "Настройка")
group1.add_argument('--rm_count', help='Количество удаляемых элементов', type=int)

group2 = parser.add_argument_group('process', "Обработка файлов")
group2.add_argument('--el_syy_path', help="Файл для целевой функции", type=str)
group2.add_argument('--points_path', help="Файл узлов", type=str)
group2.add_argument('--edges_path', help="Файл ребер", type=str)
group2.add_argument('--triangles_path', help="Файл треугольников", type=str)
group2.add_argument('--gr_path', help="Файл граничных условий", type=str)
