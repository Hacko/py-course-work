CURRENT_PATH = $(shell pwd)
ENV_PATH = $(CURRENT_PATH)/env
PYTHON_BIN = /usr/bin/python3

all: \
	var/make \
	var/install

var/make:
	@mkdir var
	touch $@

var/install:
	@echo "Creating virtualenv"
	@virtualenv -p $(PYTHON_BIN) $(ENV_PATH)
	@echo "Virtualenv created"
	@echo "Installing pip packages"
	@$(ENV_PATH)/bin/pip install -r requirements.txt
	@echo "Packages successfully installed"
	@mkdir data
	@touch $@


clean:
	@sudo rm -rf $(ENV_PATH) var
