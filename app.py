# ~*~ coding: utf-8 ~*~
from parser import parser
import logging
import helpers

logger = logging.getLogger('course_work')


def main():
    # обработка параметров
    args = parser.parse_args()

    logger.info("Run command file with params: {}".format(args))

    if args.cmd == 'conf':
        assert args.rm_count
        helpers.update_config(rm_count=args.rm_count)
        exit(0)

    if args.cmd == 'process':
        store = helpers.DataStore(args.points_path, args.edges_path, args.triangles_path, args.gr_path, args.el_syy_path)
        store.parse_files()
        store.sort_el_syy_data()
        store.process_deletion()
        store.save_processed_data()
        exit(0)


if __name__ == '__main__':
    main()
