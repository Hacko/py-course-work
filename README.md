## Параметры скрипта

### conf:
- Настройка
    - --rm_count - Количество удаляемых элементов

#### process:
- Обработка файлов
    -    --el_syy_path - Файл для целевой функции
    - --points_path - Файл узлов
    - --edges_path - Файл ребер
    - --triangles_path - Файл треугольников
    - --gr_path - Файл граничных условий

#### Примеры запуска:
- python app.py --help
- python app.py --cmd=conf --rm_count=3
- python app.py --cmd=process --el_syy_path=data/EL_Syy.txt --points_path=data/data11.txt --edges_path=data/data12.txt --triangles_path=data/data13.txt --gr_path=data/gr.txt