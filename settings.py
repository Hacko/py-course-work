import logging.config

__all__ = [
    'CONF_FILE', 'LOGGING'
]

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        }
    },
    'handlers': {
        'console':{
            'level':'DEBUG',
            'class':'logging.StreamHandler',
            'formatter': 'verbose'
        }
    },
    'loggers': {
        'course_work': {
            'handlers': ['console'],
            'level': 'INFO'
        }
    }
}


CONF_FILE = 'conf.json'

logging.config.dictConfig(LOGGING)
