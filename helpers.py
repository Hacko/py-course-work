# ~*~ coding: utf-8 ~*~
import os
import json
import logging

import settings
import exceptions

logger = logging.getLogger('course_work')


class DataLine(object):
    """
    Используется для хранения строк файлов в удобном для вычислений формате.
    
    """

    __slots__ = [
        'origin',
        'parsed',
        'position',
        'context',
        'deleted'
    ]

    def __init__(self, origin, parsed, position):
        """
        :param origin: str Оригинальная строка из файла
        :param parsed: any Данные из origin сконвертирвоанные в требуемом для вычислений формате
        :param position: int Позиция строки в файле
        """

        assert isinstance(origin, str)
        assert isinstance(position, int)

        self.origin = origin
        self.parsed = parsed
        self.position = position
        self.context = {}
        self.deleted = False

    @property
    def avg(self):
        """
        Среднее арифметическое значений
        :return: number
        """

        return sum(self.parsed) / len(self.parsed)

    def mark_as_deleted(self):
        """ Устанавливаем запрет на удаление данных """
        self.deleted = True

    def __str__(self):
        return self.origin


class DataStore(object):

    """
    Парсинг и преобразование текстовых данных.
    Включает в себя методы проверки корректности формата данных и вычисления.
    
    """

    __slots__ = [
        '_points_file_path',
        '_triangles_file_path',
        '_edges_file_path',
        '_data_el_syy_path',
        '_boundary_path',
        '_validated',
        '_deleted_count',
        '_config',
        'data'
    ]

    def __init__(self, points_file_path, edges_file_path, triangles_file_path, boundary_path, el_syy_path):
        """
        Конструктор хранилища
        
        :param points_file_path: str Файл точек
        :param edges_file_path: str Файл ребер
        :param triangles_file_path: str Файл треульников 
        :param el_syy_path: str Файл для значений целевой функции
        """

        assert isinstance(points_file_path, str)
        assert isinstance(edges_file_path, str)
        assert isinstance(triangles_file_path, str)
        assert isinstance(el_syy_path, str)
        assert isinstance(boundary_path, str)

        self._points_file_path = points_file_path
        self._edges_file_path = edges_file_path
        self._triangles_file_path = triangles_file_path
        self._data_el_syy_path = el_syy_path
        self._boundary_path = boundary_path
        self._deleted_count = 0
        self._validated = False
        self._config = get_config()
        self.data = {}

    def parse_files(self):
        """
        Проверка корректности данных, переданных для обработки.
        В случае некорректности формата или отсутствия файла, будет возбуждено исключение `exceptions.ValidationError` 
        """

        # проверка корректности форматов

        self.data['points'] = self._parse_file_content(self._points_file_path, float, 3)
        self.data['edges'] = self._parse_file_content(self._edges_file_path, int, 2)
        self.data['triangles'] = self._parse_file_content(self._triangles_file_path, int, 3)
        self.data['values'] = self._parse_el_syy_file_content()
        self.data['boundary_values'] = self._parse_file_content(self._boundary_path, int, 2)

    def _parse_file_content(self, file_path, func, sl_count):
        """
        Обработка содержимого файла.
        
        :param file_path: str Путь к файлу
        :param func: callable Функция для преобразования значений в строке
        :param sl_count: int Количество значений начиная с нулевого левого, которые нужно взять из строки
        :return: array of tuple 
        """

        data = []
        error_message = "Некорректный формат файла {}, {}"

        if not os.path.isfile(file_path):
            raise exceptions.ValidationError("Указанный файл {} не найден".format(file_path))

        with open(file_path, 'r') as f:
            lines_count = f.readline().strip()
            if not lines_count.isdigit():
                raise exceptions.ValidationError(
                    error_message.format(self._points_file_path, "первая строка должна содержать число")
                )

            lines_count = int(lines_count)
            lines = f.readlines()

            if len(lines) != lines_count:
                raise exceptions.ValidationError(
                    error_message.format(self._points_file_path, "количество строк указано неверно")
                )

            index = 0
            for line in lines:
                data.append(
                    DataLine(origin=line, parsed=line_to_number_list(line, transform=func)[:sl_count], position=index)
                )
                index += 1
        logger.info("Файл {} успешно обработан".format(file_path))
        return data

    def _parse_el_syy_file_content(self):
        """
        Чтение содержимого EL_Syy файла.
        Каждая строка файла будет преобразована в массив float.

        :return: list of DataLine
        """

        if not os.path.isfile(self._data_el_syy_path):
            raise exceptions.ELSyyNotFound("Файл EL_Syy не найден")

        values = []
        with open(self._data_el_syy_path, 'r') as f:
            index = 0
            for line in f.readlines():
                values.append(
                    DataLine(origin=line, parsed=line_to_number_list(line), position=index)
                )
                index += 1
        logger.info("Файл {} успешно обработан".format(self._data_el_syy_path))
        return values

    def get_protected_nodes(self):
        """
        Получение списка узлов, которые нельзя удалять
        :return: set of int
        """

        gr_set = set()
        for data_item in self.data['boundary_values']:
            gr_set |= set(data_item.parsed)
        return gr_set

    def sort_el_syy_data(self):
        self.data['values'] = sorted(self.data['values'], key=lambda x: x.avg)

    def process_deletion(self):
        max_deletion_size = self._config['rm_count']
        protected_nodes = self.get_protected_nodes()

        # цикл по значениям sl_syy
        for data_item in self.data['values']:

            # треугольник по индексу значения
            triangle = self.data['triangles'][data_item.position]

            edges_distances = []
            should_deleted = True

            # цикл по ребрам треугольника
            for edge_index in triangle.parsed:

                # ребро треугольника. нумерация в файлах с 1, в программе с нуля
                edge = self.data['edges'][edge_index - 1]
                edge_points = []
                for point_index in edge.parsed:

                    if point_index in protected_nodes:
                        should_deleted = False

                    edge_points.append(self.data['points'][point_index - 1].parsed)

                edges_distances.append(calc_distance(*edge_points))

            triangle.context['area'] = calc_tri_area(*edges_distances)

            # если в списке точек треугольника отсутствуют точки из гарничных значний, то удаляем запись
            if should_deleted:
                data_item.mark_as_deleted()
                self._deleted_count += 1

            if self._deleted_count >= max_deletion_size:
                break

        logger.info("Количество отмеченных элементов для удаления: {}".format(self._deleted_count))
    
    def save_processed_data(self):
        sorted_data = sorted(self.data['values'], key=lambda x: x.position)

        with open(self._data_el_syy_path, 'w') as f_el, open(self._triangles_file_path, 'w') as f_tr:
            length = str(len(self.data['values']) - self._deleted_count)
            f_tr.write('{}\n'.format(length))
            for data_item in sorted_data:
                triangle = self.data['triangles'][data_item.position]
                if not data_item.deleted:
                    f_el.write(data_item.origin)
                    f_tr.write(triangle.origin)
                else:
                    logger.info('Удалены строки с индексом {position} ("{el_raw}" => "{tr_raw}");'.format(
                        position=data_item.position,
                        el_raw=data_item.origin.strip(),
                        tr_raw=triangle.origin.strip()
                    ))


def calc_distance(point1, point2):
    """
    Подсчет дистанции между двумя точками
    
    :param point1: list or number
    :param point2: list or number
    :return: float
    """

    x1, y1, z1 = point1
    x2, y2, z2 = point2
    return ((x2 - x1)**2 + (y2 - y1)**2 + (z2 - z1)**2) ** 0.5


def calc_tri_area(a, b, c):
    """
    Площади треугольник по его ребрам.
    Используется формула Герона

    :param a: number Длина стороны a
    :param b: number Длина стороны b
    :param c: number Длина стороны c
    :return: float 
    """

    pp = (a + b + c) / 2.0
    return (pp * (pp - a) * (pp - b) * (pp - c)) ** 0.5


def get_config():
    """ Чтение файла с настройками. Файл сохраняется в формате JSON. """

    conf = {}
    if os.path.isfile(settings.CONF_FILE):
        with open(settings.CONF_FILE, 'r') as f:
            try:
                conf = json.load(f)
            except Exception as e:
                logger.info("Ошибка чтения файла: {}".format(e))
    return conf


def update_config(**params):
    """
    Обновление файла настроек `settings.CONF_FILE` проекта
    
    :param params: dict Настройки проекта подлежащие добавлению/изменению 
    :return: 
    """

    conf = get_config()
    conf.update(params)
    with open(settings.CONF_FILE, 'w') as f:
        json.dump(conf, f, indent=4)
    logger.info("Файл настроек успешно обновлен")


def line_to_number_list(line, transform=float):
    """
    Преобразование строки, содержащей список чисел разделенных двойным пробелом в float array
    Пример строки: "-.23821518478161E-05  -.23818716132048E-05  -.23823468663879E-05"
     
    :param line: str Строка для парсинга
    :param transform: callable Функция для приведения типов
    :return: float array
    """

    values = []
    for value in line.split():
        value = value.strip()
        if value:
            values.append(transform(value))

    if len(values) == 0:
        raise exceptions.EmptyLineError

    return tuple(values)


def save_data(file_path, lines):
    """
    Сохранение файла усредненных значений
    
    :param file_path: str Путь для выходного файла 
    :param lines: list of float Список усредненных значений
    :return: 
    """

    length = len(lines)
    with open(file_path, 'w') as f:
        for index, line in enumerate(lines):
            data = str(line)
            if index != length - 1:
                data = '{}\n'.format(data)
            f.write(data)
    logger.info("Файл {} успешно сохранен".format(file_path))
