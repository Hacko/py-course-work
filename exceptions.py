# ~*~ coding: utf-8 ~*~
class ELSyyNotFound(Exception):
    pass


class EmptyLineError(Exception):
    pass


class ValidationError(Exception):
    pass

